---
title: "Indéniablement"
draft: false
pubDatetime: 2023-11-07T00:21:26Z
description: "Olympe est de ces femmes qui ne se sentaient pas femme mais qui ne se sentait pas homme non plus, ni neutre, ni quoi que ce soit d’autre de nommable..."
---

Olympe est de ces femmes qui ne se sent pas femme mais qui ne se sent pas homme non plus, ni neutre, ni quoi que ce soit d’autre de nommable. Elle ne comprend pas ce que ça veut dire. Tous ces comportements, ces traits, ces caractéristiques portés à un genre. C’est trop compliqué. Indéniablement plus compliqué que d’essayer de comprendre les structures complexes des atomes pour former un élément. Olympe se voit simplement humaine, poussière d’humaine parmi les milliards d’autres. Un grain de sable. Un rien. Pourtant elle ne peut s’empêcher de fermer les yeux sur ce problème. Elle jette à peine la problématique du genre que la question sautille comme une alarme dans sa tête: « qu’est-ce qu’être humain ? ».
