---
title: "Paysage"
draft: false
pubDatetime: 2023-11-08T00:00:00Z
description: "Tu le savais toi ? Que ce monde était bien étrange, bien mystérieux, bien secret alors même que nous continuons à vivre comme si ce n’était pas le cas..."
---

Tu le savais toi ? Que ce monde était bien étrange, bien mystérieux, bien secret alors même que nous continuons à vivre comme si ce n’était pas le cas. Un peu comme lorsque nous lisons un roman à la narration atypique sans lui trouver le moindre signe de bizarrerie. Tu ne trouvais pas cela fou que les paysages de ta peau, grossis x fois au microscope, offrent des paysages semblables à ceux que nous pouvons observer sur la Terre. Je ne peux m’empêcher d’être à la fois troublé et fasciné. La magie nous entoure constamment. Elle est là partout, mais, tu sais, c’est bien souvent ce qu’il y a sous nos yeux que nous ne voyons pas.
