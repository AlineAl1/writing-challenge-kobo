---
title: "Stèle"
draft: false
pubDatetime: 2023-11-06T00:00:00Z
description: "Ingrate patrie, tu n’auras pas mes os » pouvait-on lire sur sa stèle. Quelle épitaphe puissante..."
---

« Ingrate patrie, tu n’auras pas mes os » pouvait-on lire sur sa stèle. Quelle épitaphe puissante. Même dans la mort Scipion aura su marquer l’histoire de ses derniers mots. Lui qui pourtant avait été connu pour ses exploits militaires, lui ayant laissé traîner la mort et le sang, lui qui, malgré cela, avait brillé d’une aura particulière parmi les autres hommes. N’était-ce pas ironique ? Mettre fin à des guerres par la guerre. Se mettre au service d’une patrie pour cracher sur l’autre alors même que la Terre, indifférente aux conflits humains, ravale tous les os sans la moindre différence.
