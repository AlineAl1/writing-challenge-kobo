---
title: "Mille"
draft: false
pubDatetime: 2023-11-12T00:00:00Z
description: "Comment s’y retrouver ? Comment savoir où poser mes pieds ? Dans cet univers aux mille vérités qui peut-être ne sont pas ce qu’elles sont..."
---

Comment s’y retrouver ? Comment savoir où poser mes pieds ? Dans cet univers aux mille vérités qui peut-être ne sont pas ce qu’elles sont, qui peut-être même n’existent pas. Telle une errante, Jade ne savait pas dans quelle direction aller. Elle a d’abord laissé le hasard faire le travail pour elle. Face au moindre choix, à l’image de l’homme-dé, elle lançait alternativement une pièce jouant le célèbre jeu du pile ou face. Néanmoins Jade avait la sensation de tourner en rond, emprisonnée, dans un cycle infernal. Il lui fallu des années avant de se rendre compte qu’elle n’avait pas besoin qu’on lui donne une vérité toute faite. Il lui suffisait de construire la sienne.
