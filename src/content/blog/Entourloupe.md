---
title: "Entourloupe"
draft: false
pubDatetime: 2023-11-10T00:00:00Z
description: "Tout ce merdier là ! Oui tout ça c'est une comédie ! Une scène ! Une entourloupe ! Voilà ce qu'est notre monde, notre vie, notre réalité..."
---

"Tout ce merdier là ! Oui tout ça c'est une comédie ! Une scène ! Une entourloupe ! Voilà ce qu'est notre monde, notre vie, notre réalité. Une entourloupe ! Et même si ce n'était pas le cas, ça ne changerait rien. Que ce soit vrai ou faux, ou substantiel, ou bizarre, peu importe !", disait-il souvent. Ce à quoi je lui répondais, les yeux dans le vague:

- Si ça ne change rien, alors pourquoi te mets-tu en colère ?,
- Mais t'as rien compris !, rétorquait-il aussitôt. Découvrir l'absurdité des choses c'est toute une émotion ! Ça ne se dit pas sans émotion sinon les mots ne disent rien. Les mots seuls ne disent rien ! Bordel faut vraiment tout t'expliquer !
  Quand il répondait ainsi, je me contentais de soupirer mais pas du soupir de lassitude, de celui de la réflexion profonde que ses paroles allaient entraîner.
