---
title: "Indigo"
draft: false
pubDatetime: 2023-11-11T00:00:00Z
description: "Claudiquant, tentant tant bien que mal à marcher, je me faisais le constat intérieur du dépérissement des mes vieux os..."
---

Claudiquant, tentant tant bien que mal à marcher, je me faisais le constat intérieur du dépérissement des mes vieux os. Chaque jour, je sentais mes membres se fatiguer, se distordre, perdre en efficacité. Seul cet origami en papier, que je tenais dans le creux de ma main, faisait briller cette flamme en moi à la même intensité. La sensation du papier sur la peau déclenchait un éventail de couleurs dans ma tête. Je me souvenais alors de nos nombreuses balades nocturnes. Je me souvenais oui de ces nuits noires bleutées, presque indigo, remplies de ces milliers et milliers d’astres. Nous nous sentions si petits face à cette immensité mais si grands de l’amour que nous nous portions l’un pour l’autre.
