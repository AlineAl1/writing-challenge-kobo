---
title: "Coruscant"
draft: false
pubDatetime: 2023-11-09T00:00:00Z
description: "Je suis si heureuse en ce moment. Oui, heureuse. Tellement que je ne laisse pas la moindre ombre m'atteindre..."
---

Je suis si heureuse en ce moment. Oui, heureuse. Tellement que je ne laisse pas la moindre ombre m'atteindre. Ces ombres, elles m'ont entraîné dans leur sillage sombre et gris si longtemps. Chaque jour il a fallu se maintenir la tête hors de leur mare ténébreuse alors même que mon corps s'y enlisait.
Alors je profite de cette période lumineuse, brillante, coruscante même ! Je m'aperçois de tout ce qui m'entoure. Je me réjouis de ta présence à mes côtés, de ce lien particulier.
