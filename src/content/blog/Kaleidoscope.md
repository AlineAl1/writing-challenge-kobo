---
title: "Kaléidoscope"
draft: false
pubDatetime: 2023-11-03T00:21:26Z
description: "Les mathématiques ne sont pas ce que l’on croit. Ce n’est pas cette logique froide et rigoureuse sans le moindre sens comme l’enseigne l’école..."
---

Les mathématiques ne sont pas ce que l’on croit. Ce n’est pas cette logique froide et rigoureuse sans le moindre sens comme l’enseigne l’école. Une réalité se cache derrière, une réalité certes fait de nombres, mais ces nombres peuvent se matérialiser en quelque chose de magique. C’est avec une fascination non sans enthousiasme que Benjamin en prend conscience. Chaque fois qu’il porte le kaléidoscope à ses yeux, ces multiples couleurs au sein de ces formes parfaites, symétriques, scintillantes donnent un spectacle époustouflant. Il n’y voit pas une froideur glaciale et sans vie. Au contraire il comprend le génie de cet appareil. Il donne enfin la preuve que les mathématiques ne sont pas hors de notre portée. Nous pouvons littéralement en admirer les palettes.
