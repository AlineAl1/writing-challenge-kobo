---
title: "Toundra"
draft: false
pubDatetime: 2023-11-02T00:00:00Z
description: "La vie d’Alix avait toujours été ainsi. Elle se voyait comme une guerrière. Certes abîmée par le temps, les événements, la vie tout simplement..."
---

La vie d’Alix avait toujours été ainsi. Elle se voyait comme une guerrière. Certes abîmée par le temps, les événements, la vie tout simplement. Mais existe-t-il seulement une guerrière en ce monde qui ne soit pas amochée ? Qui ne doute pas ? Qui ne se sent pas aux prises de la peur parfois ? C’est en cela qu’Alix puisait sa force. Telle la toundra survivant au froid glacial, elle se relevait chaque fois plus chargée que jamais. Elle avait pris conscience que les échecs étaient tout autant importants que les réussites. Aussi infimes pouvaient-elles être.
