---
title: "Vaciller"
draft: false
pubDatetime: 2023-11-05T00:00:00Z
description: "Marcus n’avait que six ans lorsque ses visions débutèrent. Il n’avait jamais été comme les autres..."
---

Marcus n’avait que six ans lorsque ses visions débutèrent. Il n’avait jamais été comme les autres. Il aurait bien du mal à s’expliquer en quoi mais il y avait chez ces autres qui lui ressemblent comme une nature étrangère à la sienne. Alors que les enfants, ses « camarades », jouaient dans la cours de récréation profitant du beau temps, il s’était surpris à se perdre dans la contemplation du soleil. Peu importe que ses yeux le brûlent au contact de cette chaleur intense, Marcus observait, fasciné, subjugué, stupéfait de le voir vaciller. Le soleil semblait trembler de en part en part ses rayons. Il tendit la main, l’envie soudaine de le tenir de sa paume. De se fondre en lui même. Il commença à se voir devenir soleil capable de réchauffer la planète de son étrangeté. Oui, c’était bien ainsi que tout commença, les visions lui vinrent alors même qu’il devenait aveugle.
