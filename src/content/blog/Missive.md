---
title: "Missive"
draft: false
pubDatetime: 2023-11-04T00:00:00Z
description: "Il suffit d’un rien, d’un détail, d’une minuscule particule pour que le monde se transforme, ou du moins, semble se transformer..."
---

Il suffit d’un rien, d’un détail, d’une minuscule particule pour que le monde se transforme, ou du moins, semble se transformer. Cette missive, elle fût sans conteste ce qui m’arriva de plus beau, de plus merveilleux, de plus pur dans ma vie. Nous lisons souvent que les mots sont comme des lames qui transpercent l’âme. Il est vrai. Mais toute déchirure n’est pas une blessure. Tes mots m’ont semblé être comme des ondes qui se propagent. Ils ont été lancé là, l’air de rien, couché sur le papier de tes mains géniales et je pouvais déjà ressentir les ondulations me traverser de toute part avant de s’étendre autour englobant notre monde jusqu’à l’impacter imperceptiblement.
