import type { CollectionEntry } from "astro:content";

const getLastFourPosts = (posts: CollectionEntry<"blog">[]) => {
  return posts
    .sort((a, b) => b.data.pubDatetime.getTime() - a.data.pubDatetime.getTime())
    .slice(0, 3);
};

export default getLastFourPosts;
