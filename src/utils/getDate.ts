const getDate = (date: Date) => {
  const formatter = new Intl.DateTimeFormat("fr-FR", {
    weekday: "long",
    day: "numeric",
    month: "long",
    year: "numeric",
  });
  const formattedDate = formatter.format(new Date(date));
  return formattedDate;
};

export default getDate;
