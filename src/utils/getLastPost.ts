import type { CollectionEntry } from "astro:content";

const getLastPost = (posts: CollectionEntry<"blog">[]) => {
  return posts.sort((a, b) => b.data.pubDatetime.getTime() - a.data.pubDatetime.getTime())[0];
};

export default getLastPost;
